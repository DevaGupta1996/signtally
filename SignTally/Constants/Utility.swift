//
//  Utility.swift
//  HeroNavigation
//
//  Created by orangemac05 on 18/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import CoreLocation
import GoogleMaps
import SafariServices

func startNotifier(_ type : String){
    let nc = NotificationCenter.default
    nc.post(name:NSNotification.Name(rawValue: type), object: nil, userInfo:nil)
}

func setStatusBar(){
    let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
    if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
        statusBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
}

func compressImage(image: UIImage?) -> UIImage? {
    guard let image = image else {
        return nil
    }
    let rect = CGRect(x: 0, y: 0, width: image.size.width/3, height: image.size.height/3)
    UIGraphicsBeginImageContext(rect.size)
    image.draw(in: rect)
    let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    let compressedImageData = resizedImage?.jpegData(compressionQuality: 0.1)
    return UIImage(data: compressedImageData!)!
}

func showAlert(title: String?, message: String?, buttonTitle: String?, vc: UIViewController) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    alert.view.tintColor =  #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
    let action = UIAlertAction(title: buttonTitle, style: .cancel, handler: nil)
    alert.addAction(action)
    vc.present(alert, animated: true, completion: nil)
}

typealias AlertCompletion = (_ action: UIAlertAction)->(Void)
typealias CancelCompletion = (_ action: UIAlertAction)->(Void)

func showAlertWithCompletion(title: String?, message: String?, buttonTitle: String?, vc: UIViewController, completion:@escaping AlertCompletion) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    alert.view.tintColor =  #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
    let action = UIAlertAction(title: buttonTitle, style: .default, handler: completion)
    alert.addAction(action)
    vc.present(alert, animated: true, completion: nil)
}

func showPromptWithCompletion(title: String?, message: String?, buttonTitles: [String]?, vc: UIViewController, completion:@escaping AlertCompletion, cancel:@escaping CancelCompletion) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    alert.view.tintColor =  #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
    let action = UIAlertAction(title: buttonTitles?[0], style: .default, handler: completion)
    alert.addAction(action)
    
    let action2 = UIAlertAction(title: buttonTitles?[1], style: .default, handler: cancel)
    alert.addAction(action2)
    vc.present(alert, animated: true, completion: nil)
}

func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,3}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}


func USER_ID() -> String{
    if let userId = Preferences.getUserID(){
        print(">>>>>>>>>\(userId)<<<<<<<<<")
        return userId
    }
    return ""
}

func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}


func dateWithTZ(fromString: String) -> Date? {
    
    let dateString = fromString
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    
    guard let dateObj = dateFormatter.date(from: dateString) else {
        return nil
    }
    return dateObj
}


func dateSignTally(fromString: String) -> Date? {
    let dateString = fromString
    let dateFormatter = DateFormatter()
    
    dateFormatter.dateFormat = "yyyy-MM-dd"
    
    guard let dateObj = dateFormatter.date(from: dateString) else {
        return nil
    }
    return dateObj
}


func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
    CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { placemarks, error in
        guard let placemark = placemarks?.first, error == nil else {
            completion(nil, error)
            return
        }
        completion(placemark, nil)
    }
}

func roundOffValue(_ inputValue : Double) -> Double{
    let y = Double(round(1000*inputValue)/1000)
    print(y)  // 1.236
    return y
}


func openSafariController(_ viewController : UIViewController ,_ urlString : String,_ source : String){
//    if let url = URL(string: urlString) {
//        let config = SFSafariViewController.Configuration()
//        config.entersReaderIfAvailable = true
//        let vc = SFSafariViewController(url: url, configuration: config)
//        viewController.present(vc, animated: true)
//    }
    
    if urlString != ""{
        if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.PDF_DISPLAY_IDENTIFIER)as? PdfDisplayController{
            vc.pdfLink = urlString
            vc.source = source
            viewController.present(vc, animated: true, completion: nil)
        }
    }else{
        showAlert(title: "SignTally", message: "No pdf available", buttonTitle: "Ok", vc: viewController)
    }
}


func getCommaSepratedNumber(_ number : Double) -> String{
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.locale = Locale(identifier: "en_US") // English (India) locale
    let string = formatter.string(for: number)!
    return string
}
