//
//  Constants.swift
//  GoogleMapDemo
//  Created by Orange on 18/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import UIKit

var GOOGLE_MAP_API_KEY = /*"AIzaSyB6fYX9tbBilYG18hgEl6kyRnfyxLqT8n0"*/"AIzaSyCg0i1I6Cuwf0kFjt98lXpKBUKO3zkWlkU"
//let GOOGLE_ADDRESS_API = "https://maps.googleapis.com/maps/api/geocode/json?"
//let BASE_URL_GEOCODE = "https://maps.googleapis.com/maps/api/geocode/json?"
//let BASE_URL_DIRECTIONS = "https://maps.googleapis.com/maps/api/directions/json?"

let IMAGE_UPLOAD_URL = "https://api.signtally.com/upload/docs"
let BASE_URL = /*"http://ec2-18-222-92-210.us-east-2.compute.amazonaws.com:3000/"*/ "https://api.signtally.com/"   //
let BASE_COST_URL = "https://api.signtally.com/schedule/base/cost"
let DEVICE_TOKEN = "360df891da55f5b0cbf6f609a0aa92f26ca358851fbefbea6770f17097f3389b"
let past_Valuation_API = "https://api.signtally.com/past/valuation"


protocol DataPickerDelegate {
    func pickupData(selected: String,selectedIndex: Int,sourceName:String)
}

let MAIN_STORYBOARD = UIStoryboard(name: "Main", bundle: nil)
let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate





struct VIEW_CONTROLLER_IDENTIFIER{
    static let HOME_NAVIGATION_IDENTIFIER = "homeNav"
    static let HOME_IDENTIFIER = "home"
    static let LOGIN_IDENTIFIER = "Login"
    static let SIGNUP_IDENTIFIER = "signUp"
    static let FORGOT_PASSWORD_IDENTIFIER = "forgot"
    static let ADD_SIGN_IDENTIFIER = "addSign"
    static let SIGN_RESULT_DISPLAY_IDENTIFIER = "resultDisplay"
    static let PAST_VALUATIONS_IDENTIFIER = "pastValuation"
    static let CHANGE_PASSWARD_IDENTIFIER = "changePassward"
    static let HELP_CONTROLLER_IDENTIFIER = "help"
    static let DATA_PICKER_IDENTIFIER = "normalPicker"
    static let MAP_IDENTIFIER = "map"
    static let WEB_IDENTIFIER = "web"
    static let PDF_DISPLAY_IDENTIFIER = "pdfDisplay"
    static let PRIVACY_POLICY_INDENTIFIER = "privacy"
}

struct SIDE_MENU_NOTIFIER {
    static let ADD_SIGN = "AddSign"
    static let PAST_VALUATION = "PastValuations"
    static let HELP = "Help"
    static let PRIVACY_POLICY = "privacy"
    static let CHANGEPASSWARD = "changePassward"
    static let LOGOUT = "logout"
}

struct VALIDATION_ENUM{
    //USER VALIDATIONS
    static let ALL_FIELD_VALIDATION_MESSEGE = "All Fields Required."
    static let BLANK_EMAIL_VALIDATION_MESSEGE =  "Please enter email address"
    static let INVALID_EMAIL_MESSEGE = "Please enter valid email address"
    static let BLANK_PASSWARD_VALIDATION_MESSEGE = "Please enter your password"
    static let INVALID_PASSWARD_MESSEGE = "Please enter your valid password"
    static let BLANK_FIRST_NAME_VALIDATION_MESSEGE =  "Please enter your first name"
    static let BLANK_LAST_NAME_VALIDATION_MESSEGE =  "Please enter your last name"
    static let BLANK_CONFIRM_PASSWORD_VALIDATION_MESSEGE =  "Please enter your confirm password "
    static let BLANK_CHECK_PASSWORD_VALIDATION_MESSEGE =  "Password does not match "
    static let BLANK_CURRENT_PASSWORD_VALIDATION_MESSEGE =  "Please enter your current password"
    static let BLANK_NEW_PASSWORD_VALIDATION_MESSEGE =  "Please enter your new password"
    static let CHECK_T_AND_C = "Please accept Terms & Conditions"
    
    
    //IDENTIFY VALIDATIONS
    static let SIGN_OWNER_VALIDATION_MSG = "Please select sign owner"
    static let OTHER_SIGN_OWNER_VALIDATION_MSG = "Please mention other compant name"
    static let LOCATION_VALIDATION_MSG = "Please specify location of sign"
    static let SIGN_IMAGE_VALIDATION_MSG = "Please provide sign picture"
    
    //FEATURES VALIDATIONS
    static let SUB_TYPE_VALIDATION_MSG = "Please mention subtype "
    static let DISPLAY_SIZE_VALIDATION_MSG = "Please mention display size"
    static let HAGL_VALIDATION_MSG = "Please specify HAGL"
    static let ILLUMINATED_VALIDATION_MSG = "Please specify illuminatiion type"
    static let YEAR_BUILT_VALIDATION_MSG = "Please specify year built"
    static let AGE_VALIDATIN_MSG = "Please specify age"
    
    // DIGITAL DISPLAY VALIDATIONS
    static let DIGITAL_DISPLAY_VALIDATION = "Please specify digital display"
    static let YEAR_BUILD_DISPLAY_VALIDATION = "Please specify build display year"
    static let THREE_MESSEGE_DISPLAY_VALIDATION = "Please specify three messege display"
    static let YEAR_THREE_MSG_DISPLAY_VALIDATION = "Please specify year of three message"
    
}


struct WEB_SERVICE_URL {
    static  let signUp = "users"
    static let login = "users/login"
    static let forgot = "users/forget/password"
    static let changePassword = "users/change/password"
    static let billboardcompanyinfo = "billboard/company/info"
    static let billboard = "billboard"
}


struct DROP_DOWN_ARRAYS{
    static  let SIGN_OWNER_ARRAY = ["Clear Channel", "Outfront Media", "Lamar Advertising","Other"]
    static let STRUCTURE_TYPE_ARRAY = ["Wood","Steel A Frame","Steel Multi-Mast","Steel Monopole"]
    static let DISPLAY_SIZE_ARRAY = ["300", "378", "480", "672"]
    static let HAGL_ARRAY = ["0-20", "21-30", "31-40","41-55"]
    static let DISPLAY_ELEMENT_ARRAY = ["Any Stacked", "Side by Side","Both","None"]
    static let ILLUMINATION_TYPE_ARRAY = ["Not Illuminated", "Illuminated"]
    static let YEAR_BUILT_ARRAY =
 ["2019","2018","2017","2016","2015","2014","2013","2012","2011","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000","1999","1998","1997","1996","1995","1994","1993","1992","1991","1990","1989","1988","1987","1986","1985","1984","1983","1982","1981","1980","1979","1978","1977","1976","1975","1974","1973","1972","1971","1970","1969","1968","1967","1966"]
    
    static let YEAR_BUILT_ARRAY2 = ["2019","2018","2017","2016","2015","2014","2013","2012","2011","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000"]
    
    static let DIGITAL_DISPLAY_ARRAY = ["0","1","2","3","4","5"]
}

struct  BUTTON_TYPE_ENUM{
    static let STRUCTURE_TYPE = "kStructureType"
    static let SUB_TYPE = "kSubType"
    static let DISPLAY_SIZE_TYPE = "kDisplaySizeType"
    static let HAGL_TYPE = "kHAGL_Type"
    static let DISPLAY_ELEMENT_TYPE = "kDisplayElementType"
    static let ILLUMINATION_TYPE = "killuminationType"
    static let YEAR_BUILT_TYPE = "kBuiltType"
    
    static let DIGITAL_DISPLAY_TYPE = "kDigitalDisplay"
    static let YEAR_BUILD_DISPLAY_TYPE = "kBuildDisplayType"
    static let THREE_MESSEGE_DISPLAY_TYPE = "kMessegeDisplay"
    static let YEAR_THREE_MSG_TYPE = "kYearThreeMsgType"
}



struct WOOD {
    static let SIZE = ["300", "378", "480", "672"]
    static let  SUB_TYPE = ["Single Face","Double Face", "V Built and Side by Side"]
    static let HAGL = ["0-20", "21-30", "31-40","41-55"]
}


struct STEEL {
    static let SIZE = ["300","378"]
    static let  SUB_TYPE = ["Single Face","Double Face","V Built"]
    static let HAGL = ["0-20", "21-30", "31-40"]
}

struct STEEL_MM{
    static let SIZE = ["300", "378", "480", "672"]
    static let  SUB_TYPE = ["Single Face","Double Face","V Built"]
    static let HAGL = ["0-20", "21-30", "31-40","41-55"]
}

struct MONOPOLE {
    static let SIZE = ["300", "378", "480", "672","960","1000"]
    static let  SUB_TYPE = ["Single Face Center Mounted",
                            "Single Face Partial Flag",
                            "Single Face Full Flag",
                            "Double & V Face Center Mounted",
                            "Double & V Face Partial Flag",
                            "Double & V Face Full Flag",
                            "Tri-Sided Center Mounted",
                            "Tri-Sided Stacked Center Mounted"]
    static let HAGL = ["0-20", "21-30", "31-40","41-55","56-80","80+"]
}
