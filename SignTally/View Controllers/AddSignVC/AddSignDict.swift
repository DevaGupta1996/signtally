//
//  AddSignDict.swift
//  SignTally
//
//  Created by Orange on 30/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import ObjectMapper


struct ImageResponse: Mappable,Codable{
    var status = ""
    var msg = ""
    var data = [""]
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        status <- map["status"]
        msg <- map["msg"]
        data <- map["data"]
    }
}
