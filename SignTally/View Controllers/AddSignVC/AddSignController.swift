//
//  AddSignController.swift
//  SignTally
//
//  Created by Orange on 29/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import SVProgressHUD
import SafariServices
import NVActivityIndicatorView

protocol PageCountDelegate {
    func index(at: Int)
}

class AddSignController: UIViewController {
    var indicator: NVActivityIndicatorView!
    var locationManager = CLLocationManager()
    var currnetLocation = CLLocation()
    var geocoder = GMSGeocoder()
    var imageURL = ""
    var selectedImage : UIImage!
    var imagePicker = UIImagePickerController()
    var currentImage = ""
    var locationArrayGoogle = [String]()
    
    var billboardParams = [String : Any]()
    var inventory_id = ""
    var zipcode = ""
    var is_deleted = true
    var isFirstTime = true
    
    var subTypeDefaultText = ""
    var displaySizeDefaultText = ""
    var HAGL_DefaultText = ""
    
    var yearDisplayDefaultText = ""
    var yearThreeMsgDisplayDefaultText = ""
    
    var totalBaseCost : Total_Base_cost?
    
    var resultDisplayData : DataObject?
    
    var waitForCreatePDF: Bool = false
    var doct_URL = ""
    
    //    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var identifyButton: UIButton!
    @IBOutlet weak var featuresButton: UIButton!
    @IBOutlet weak var digitalButton: UIButton!
    @IBOutlet weak var commentsButton: UIButton!
    
    
    // IDENTIFY SCROLL VIEW ELEMENTS
    @IBOutlet weak var IdentifyScrollView: UIScrollView!
    @IBOutlet weak var signOwnerLabel: UILabel!
    @IBOutlet weak var customCompanyTextField: UITextField!
    @IBOutlet weak var customCompanyHeight: NSLayoutConstraint!
    @IBOutlet weak var displayNumberTextField: UITextField!
    @IBOutlet weak var firstPhotoLabel: UILabel!
    @IBOutlet weak var secondPhotoLabel: UILabel!
    @IBOutlet weak var streetNumberTextFiled: UITextField!
    @IBOutlet weak var streetNameTextField: UITextField!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var stateNameLabel: UILabel!
    @IBOutlet weak var permitNumberTextField: UITextField!
    
    // FEATURES SCROLL VIEW ELEMENTS
    @IBOutlet weak var featuresScrollView: UIScrollView!
    @IBOutlet weak var structureTypeLabel: UILabel!
    @IBOutlet weak var subTypeLabel: UILabel!
    @IBOutlet weak var displaySizeLabel: UILabel!
    @IBOutlet weak var HAGL_Label: UILabel!
    @IBOutlet weak var displayElementsLabel: UILabel!
    @IBOutlet weak var additionalCostTextField: UITextField!
    @IBOutlet weak var baseCostTextField: UITextField!
    @IBOutlet weak var illuminatedLabel: UILabel!
    @IBOutlet weak var yearBuiltLabel: UILabel!
    @IBOutlet weak var ageTextField: UITextField!
    
    @IBOutlet weak var structureTypeButton: UIButton!
    @IBOutlet weak var subTypeButton: UIButton!
    @IBOutlet weak var displaySizeButton: UIButton!
    @IBOutlet weak var haglButton: UIButton!
    
    // DIGITAL SCROLL VIEW ELEMENTS
    @IBOutlet weak var digitalScrollView: UIScrollView!
    @IBOutlet weak var digitalDisplayLabel: UILabel!
    @IBOutlet weak var yearDisplayBuiltLabel: UILabel!
    @IBOutlet weak var threeMsgDisplayLabel: UILabel!
    @IBOutlet weak var yearThreeMsgDisplayBuiltLabel: UILabel!
    @IBOutlet weak var yearThreeMsgDisplayBuildHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var yearDisplayBuildHeightConstraint: NSLayoutConstraint!
    
    
    // COMMENTS SCROLL VIEW ELEMENTS
    @IBOutlet weak var commentsScrollView: UIScrollView!
    @IBOutlet weak var commentTextView: UITextView!
    var currentIndex = 0
    var firstImageLink = ""
    var secondImageLink = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        layout.itemSize = self.getSizeAccordingToScreenSize()
        //        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        //        layout.minimumLineSpacing = 1
        //        layout.minimumInteritemSpacing = 0
        //        self.collectionViewTest.collectionViewLayout = layout
       // self.indicator = NVActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100), type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.red, padding: 10)
       // self.indicator.center = self.view.center
        
        self.subTypeDefaultText = self.subTypeLabel.text!
        self.displaySizeDefaultText = self.displaySizeLabel.text!
        self.HAGL_DefaultText = self.HAGL_Label.text!
        
        
        self.yearDisplayDefaultText = self.yearDisplayBuiltLabel.text!
        self.yearThreeMsgDisplayDefaultText = self.yearThreeMsgDisplayBuiltLabel.text!
        
        self.identifyButton.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
        self.featuresButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.digitalButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.commentsButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = true
        
        NotificationCenter.default.addObserver(forName:  NSNotification.Name(rawValue: "POPTOROOT"), object: nil, queue: OperationQueue.current) { (notification) in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupLocationManager()
    }
    
    
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        switch self.currentIndex {
        case 0:
            self.navigationController?.popViewController(animated: true)
            break
        case 1:
            self.setScrollToIndex(0)
            break
        case 2:
            self.setScrollToIndex(1)
            break
        case 3:
            self.setScrollToIndex(2)
            break
        default:
            break
        }
    }
    
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        switch self.currentIndex {
        case 0:
            if self.identifyViewValidations() == ""{
                self.setScrollToIndex(self.currentIndex + 1)
            }else{
                showAlert(title: "SignTally", message: self.identifyViewValidations(), buttonTitle: "OK", vc: self)
            }
            
            break
        case 1:
            if self.featuresViewValidations() == ""{
                self.setScrollToIndex(self.currentIndex + 1)
            }else{
                showAlert(title: "SignTally", message: self.featuresViewValidations(), buttonTitle: "OK", vc: self)
            }
            break
        case 2:
            if self.digitalViewValidations() == ""{
                self.createValuation()
                self.setScrollToIndex(self.currentIndex + 1)
            }else{
                showAlert(title: "SignTally", message: self.digitalViewValidations(), buttonTitle: "OK", vc: self)
            }
            break
        case 3:
            self.updateValuation()
            break
        default:
            break
        }
    }
    
    @IBAction func topButtonAction(_ sender: UIButton) {
        switch self.currentIndex {
        case 0:
            if sender.tag == 0{
                self.setScrollToIndex(sender.tag)
            }else{
                if self.identifyViewValidations() == ""{
                    self.setScrollToIndex(self.currentIndex + 1)
                }else{
                    showAlert(title: "SignTally", message: self.identifyViewValidations(), buttonTitle: "OK", vc: self)
                }
            }
            break
        case 1:
            if sender.tag == 0 || sender.tag == 1{
                self.setScrollToIndex(sender.tag)
            }else{
                if self.featuresViewValidations() == ""{
                    self.setScrollToIndex(self.currentIndex + 1)
                }else{
                    showAlert(title: "SignTally", message: self.featuresViewValidations(), buttonTitle: "OK", vc: self)
                }
            }
            break
        case 2:
            if sender.tag == 0 || sender.tag == 1 || sender.tag == 2{
                self.setScrollToIndex(sender.tag)
            }else{
                if self.digitalViewValidations() == ""{
                    self.createValuation()
                    self.setScrollToIndex(self.currentIndex + 1)
                }else{
                    showAlert(title: "SignTally", message: self.digitalViewValidations(), buttonTitle: "OK", vc: self)
                }
            }
            break
        case 3:
            if sender.tag == 0 || sender.tag == 1 || sender.tag == 2 || sender.tag == 3{
                self.setScrollToIndex(sender.tag)
            }else{
                self.updateValuation()
            }
            break
        default:
            break
        }
    }
    
    // IDENTIFY DROPDOWN ACTIONS
    @IBAction func signOwnerAction(_ sender: UIButton) {
        if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.DATA_PICKER_IDENTIFIER)as? DataPickerController{
            vc.genderData = DROP_DOWN_ARRAYS.SIGN_OWNER_ARRAY
            vc.genderDelegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    // IDENTIFY IMAGE SELECTION ACTIONS
    @IBAction func imageSelectorAction(_ sender: UIButton) {
        if sender.tag == 0{
            self.currentImage = "Image1"
        }else{
            self.currentImage = "Image2"
        }
        self.actionSheetButton()
    }
    
    // IDENTIFY RESET LOCATION ACTIONS
    @IBAction func resetLocationAction(_ sender: UIButton) {
        self.googleReversedGeocode(self.currnetLocation.coordinate)
    }
    
    // FEATURES DROPDOWN ACTIONS
    @IBAction func featuresDropDownAction(_ sender: UIButton) {
        if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.DATA_PICKER_IDENTIFIER)as? DataPickerController{
            vc.genderDelegate = self
            self.setDataDictForDropDown(sender.tag, vc)
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    // DIGITAL DROPDOWN ACTIONS
    @IBAction func digitalDiaplayAction(_ sender: UIButton) {
        if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.DATA_PICKER_IDENTIFIER)as? DataPickerController{
            vc.genderDelegate = self
            self.setDigitalDataForDropDown(sender.tag, vc)
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    // COMMENTS VIEW ACTIONS
    @IBAction func doneButtonAction(_ sender: UIButton) {
        self.updateValuation()
    }
    
    
    func setScrollToIndex(_ index : Int){
        switch index {
        case 0:
            self.currentIndex = index
            self.identifyButton.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
            self.featuresButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.digitalButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.commentsButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.IdentifyScrollView.alpha = 1
            self.featuresScrollView.alpha = 0
            self.digitalScrollView.alpha = 0
            self.commentsScrollView.alpha = 0
        case 1:
            self.currentIndex = index
            self.featuresButton.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
            self.identifyButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.digitalButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.commentsButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.IdentifyScrollView.alpha = 0
            self.featuresScrollView.alpha = 1
            self.digitalScrollView.alpha = 0
            self.commentsScrollView.alpha = 0
            
        case 2:
            self.currentIndex = index
            self.digitalButton.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
            self.featuresButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.identifyButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.commentsButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.IdentifyScrollView.alpha = 0
            self.featuresScrollView.alpha = 0
            self.digitalScrollView.alpha = 1
            self.commentsScrollView.alpha = 0
            
        case 3:
            self.currentIndex = index
            self.commentsButton.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
            self.featuresButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.digitalButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.identifyButton.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .normal)
            self.IdentifyScrollView.alpha = 0
            self.featuresScrollView.alpha = 0
            self.digitalScrollView.alpha = 0
            self.commentsScrollView.alpha = 1
            
        default:
            break
        }
    }
    
    
    
    func createValuation(){
        
        SVProgressHUD.show()
      //  self.view.addSubview(self.indicator)
       // self.indicator.startAnimating()
        
        let lat = Double(self.currnetLocation.coordinate.latitude)
        let long = Double(self.currnetLocation.coordinate.longitude)
        var year_digital_built = ""
        if  self.digitalDisplayLabel.text == "0"{
            year_digital_built = ""
        }else{
            year_digital_built = "\(getAge(self.yearDisplayBuiltLabel.text!))"
        }
        var yearThreeMsgAge = ""
        if self.threeMsgDisplayLabel.text == "0"{
            yearThreeMsgAge = ""
        }else{
            yearThreeMsgAge = "\(getAge(self.yearThreeMsgDisplayBuiltLabel.text!))"
        }
        
        var yearDigitalBuildValue = ""
        if  self.digitalDisplayLabel.text == "0"{
            yearDigitalBuildValue = ""
        }else{
            yearDigitalBuildValue = self.yearDisplayBuiltLabel.text!
        }
        
        var yearThreeMsgValue = ""
        
        if self.threeMsgDisplayLabel.text == "0"{
            yearThreeMsgValue = ""
        }else{
            yearThreeMsgValue = self.yearThreeMsgDisplayBuiltLabel.text!
        }
        
        var stacked = ""
        if self.displayElementsLabel.text == "Stacked/Side-by-Side"{
            stacked = ""
        }else{
            stacked = self.displayElementsLabel.text!
        }
        
        
        let params = [
            "user_id" : USER_ID(),
            "company_id" : "5b2e3fafbe01bc2fee1622c6",
            "sigh" : self.signOwnerLabel.text!,
            "parcel_no" : "",
            "other_company" : self.customCompanyTextField.text ?? "",
            "display_number" : self.displayNumberTextField.text ?? "",
            "permit_number" : self.permitNumberTextField.text ?? "",
            "photo_url" : self.firstImageLink,
            "photo_url_2" : self.secondImageLink,
            "longitude" : "\(roundOffValue(long))",
            "latitude" : "\(roundOffValue(lat))",
            "city" : self.locationNameLabel.text!,
            "zip_code" : self.zipcode,
            "state" : self.stateNameLabel.text!,
            "street_no" : self.streetNumberTextFiled.text!,
            "street_name" : self.streetNameTextField.text!,
            "structure_type" : self.structureTypeLabel.text!,
            "sub_type" : self.subTypeLabel.text!,
            "display_size" : self.displaySizeLabel.text!,
            "hagl" : self.HAGL_Label.text!,
            "display_element" : stacked,
            "additional_cost" : self.additionalCostTextField.text ?? "0",
            "base_cost" : self.baseCostTextField.text ?? "",
            "illuminated" : self.illuminatedLabel.text!,
            "year_built" : self.yearBuiltLabel.text!,
            "age" : "\(self.getAge(self.yearBuiltLabel.text!))",
            "digital_display" : self.digitalDisplayLabel.text!,
            "year_digital_built" : yearDigitalBuildValue,
            "three_message_display" : self.threeMsgDisplayLabel.text!,
            "year_three_message_display" : yearThreeMsgValue,
            "is_deleted": true,
            "digital_display_age" : year_digital_built,
            "three_msg_age" : yearThreeMsgAge
            ] as [String : Any]
        
        self.billboardParams = params
        print(">>>>>>>>>PDF\(self.billboardParams)")
        
        BillboardWebServices.createValuation(params) { (success, error, userData) in
            SVProgressHUD.dismiss()
            //self.indicator.removeFromSuperview()
            if success{
                if let tbc = userData?.data?.total_bast_cost{
                    self.totalBaseCost = tbc
                }
                
                if let data = userData?.data?.billboard_details{
                    print(data)
                    self.inventory_id = data._id
                    if let resultDisplay = userData?.data{
                        self.resultDisplayData = resultDisplay
                    }
                }
            }else{
                showAlert(title: "SignTally", message: error!, buttonTitle: "OK", vc: self)
            }
        }
    }
    
    
    
    func updateValuation(){
        SVProgressHUD.show()
        //self.view.addSubview(self.indicator)
       // self.indicator.startAnimating()
        self.perform(#selector(showAlertView), with: nil, afterDelay: 1)
        self.billboardParams["comments"] = self.commentTextView.text ?? ""
        self.billboardParams["inventory_id"] = self.inventory_id
        self.billboardParams["is_deleted"] = true
        if let data = self.totalBaseCost{
            self.billboardParams["structure_rcnld"] = "\(data.structure_rcnld)"
            self.billboardParams["digital_display_rcnld"] = "\(data.digital_display_rcnld)"
            self.billboardParams["three_msg_display_rcnld"] = "\(data.three_msg_display_rcnld)"
            self.billboardParams["total_billboard_rcnld"] = "\(data.total_billboard_evaluation)"
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.finaldata = self.billboardParams
        
        print(self.billboardParams)
        BillboardWebServices.updateValuation(self.billboardParams) { (success, error, billboardData) in
            if success{
                if let billboard = billboardData?.data2{
                    print((billboard.document_url))
                    self.doct_URL = billboard.document_url
                    if !self.waitForCreatePDF {
                        openSafariController(self, self.doct_URL, "")
                    }
                    //                    if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.SIGN_RESULT_DISPLAY_IDENTIFIER)as? ResultDisplayController{
                    //                        if let finalData = self.resultDisplayData{
                    //                            vc.billboardData = finalData.billboard_details
                    //                            vc.comment = billboardData?.data2?.comments ?? ""
                    //                            vc.total_bast_cost = finalData.total_bast_cost
                    //                        }
                    //                        self.navigationController?.pushViewController(vc, animated: true)
                    //                    }
                }
            }else{
                showAlert(title: "SignTally", message: error!, buttonTitle: "OK", vc: self)
            }
        }
    }
    
    
    @objc func showAlertView(){
        self.waitForCreatePDF = true
        SVProgressHUD.dismiss()
        //self.indicator.removeFromSuperview()
       // let alert = UIAlertController(title: "Signtally will save the valuation.", message: nil, preferredStyle: .alert)
       // alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (ok) in
            if self.doct_URL != "" {
                openSafariController(self, self.doct_URL, "")
            } else {
                SVProgressHUD.show()
                //self.view.addSubview(self.indicator)
                //self.indicator.startAnimating()
                self.waitForCreatePDF = !self.waitForCreatePDF
            }
       // }))
        //alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
      //  self.present(alert, animated: true, completion: nil)
    }
    
}
