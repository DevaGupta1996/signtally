//
//  AddSignWebServices.swift
//  SignTally
//
//  Created by Orange on 30/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper

class AddSignWebService{
    class func uploadIamge(_ imageToUpload : UIImage,completion: @escaping (_ success: Bool,_ message: String?,_ imageUploadData: [String]?)->()){
        
        let parameters = [
            "files" : "swift_file.jpeg"
        ]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageToUpload.jpegData(compressionQuality: 1.0)!, withName: "files", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)! , withName: key)
            }
        }, to:IMAGE_UPLOAD_URL)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    //print(">>>>>>>>>\(response.data)")
                    do {
                        let json = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as? [String:Any]
                       // let posts = json["posts"] as? [[String: Any]] ?? []
                        if let finalResponse = json{
                            print("finalResponse>>>>>>>>> \(finalResponse)")
                            if finalResponse["status"]as! String == "200"{
                                let dataArray = finalResponse["data"]
                                completion(true, nil , dataArray as! Array)
                            }else{
                                completion(false, finalResponse["message"] as! String, nil)
                            }
                        }
                    } catch let error as NSError {
                        print(error)
                        completion(false, error.localizedDescription, nil)
                    }
                }
                
            case .failure(let error):
                print(error.localizedDescription)
                 completion(false, error.localizedDescription, nil)
                //completion(false,error.localizedDescription,"")
                break
                //print encodingError.description
            }
        }
    }
}
