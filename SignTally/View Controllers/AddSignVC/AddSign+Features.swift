//
//  AddSign+Features.swift
//  SignTally
//
//  Created by Orange on 31/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import SVProgressHUD
import UIKit


extension AddSignController{
    
    func featuresViewValidations() -> String{
        if self.structureTypeLabel.text == "Structure Type" && self.subTypeLabel.text ==  "Sub Type" && self.displaySizeLabel.text == "Display Size (Upto X sf)" && self.HAGL_Label.text == "HAGL(Height above ground level)" && self.illuminatedLabel.text == "Illumination" && self.yearBuiltLabel.text == "Year Built (Estimate)"{
            return VALIDATION_ENUM.ALL_FIELD_VALIDATION_MESSEGE
        }else if self.subTypeLabel.text == "Sub Type"{
            return VALIDATION_ENUM.SUB_TYPE_VALIDATION_MSG
        }else if self.displaySizeLabel.text == "Display Size (Upto X sf)"{
            return VALIDATION_ENUM.SUB_TYPE_VALIDATION_MSG
        }else if self.HAGL_Label.text == "HAGL(Height above ground level)"{
            return VALIDATION_ENUM.HAGL_VALIDATION_MSG
        }else if self.illuminatedLabel.text == "Illumination"{
            return VALIDATION_ENUM.ILLUMINATED_VALIDATION_MSG
        }else if self.yearBuiltLabel.text == "Year Built (Estimate)"{
            return VALIDATION_ENUM.YEAR_BUILT_VALIDATION_MSG
        }
        //        else if self.ageTextField.text == ""{
        //            //return VALIDATION_ENUM.AGE_VALIDATIN_MSG
        //        }
        return ""
    }
    
    
    func setDropDownDataForFeatures(_ selected : String , selectedIndex : Int, sourceName: String){
        if sourceName == BUTTON_TYPE_ENUM.STRUCTURE_TYPE{
            self.structureTypeLabel.text = selected
            self.structureTypeLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.subTypeLabel.text = self.subTypeDefaultText
            self.displaySizeLabel.text = self.displaySizeDefaultText
            self.HAGL_Label.text = self.HAGL_DefaultText
            self.subTypeLabel.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.displaySizeLabel.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.HAGL_Label.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.baseCostTextField.text = ""
            self.subTypeButton.isUserInteractionEnabled = true
        }else if sourceName == BUTTON_TYPE_ENUM.SUB_TYPE{
            self.subTypeLabel.text = selected
            self.subTypeLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.displaySizeLabel.text = self.displaySizeDefaultText
            self.HAGL_Label.text = self.HAGL_DefaultText
            self.displaySizeLabel.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.HAGL_Label.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.baseCostTextField.text = ""
            self.displaySizeButton.isUserInteractionEnabled = true
        }else if sourceName == BUTTON_TYPE_ENUM.DISPLAY_SIZE_TYPE{
            self.displaySizeLabel.text = selected
            self.displaySizeLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.HAGL_Label.text = self.HAGL_DefaultText
            self.HAGL_Label.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            self.baseCostTextField.text = ""
            self.haglButton.isUserInteractionEnabled = true
        }else if sourceName == BUTTON_TYPE_ENUM.HAGL_TYPE{
            self.HAGL_Label.text = selected
            self.HAGL_Label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }else if sourceName == BUTTON_TYPE_ENUM.DISPLAY_ELEMENT_TYPE{
            self.displayElementsLabel.text = selected
            self.displayElementsLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }else if sourceName == BUTTON_TYPE_ENUM.ILLUMINATION_TYPE{
            self.illuminatedLabel.text = selected
            self.illuminatedLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }else if sourceName == BUTTON_TYPE_ENUM.YEAR_BUILT_TYPE{
            self.yearBuiltLabel.text = selected
            self.yearBuiltLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        if isAllFourAvailable(){
            self.calculateBaseCost()
        }
    }    
    
    func setDataDictForDropDown(_ tag : Int,_ vc : DataPickerController){
        switch tag {
        case 0:
            vc.genderData = DROP_DOWN_ARRAYS.STRUCTURE_TYPE_ARRAY
            vc.sourceView = BUTTON_TYPE_ENUM.STRUCTURE_TYPE
            break
        case 1:
            if self.structureTypeLabel.text == "Wood"{
                vc.genderData = WOOD.SUB_TYPE
            }else if self.structureTypeLabel.text == "Steel A Frame"{
                vc.genderData = STEEL.SUB_TYPE
            }else if self.structureTypeLabel.text == "Steel Multi-Mast"{
                vc.genderData = STEEL_MM.SUB_TYPE
            }else if self.structureTypeLabel.text == "Steel Monopole"{
                vc.genderData = MONOPOLE.SUB_TYPE
            }
            vc.sourceView = BUTTON_TYPE_ENUM.SUB_TYPE
            
            break
        case 2:
            if self.structureTypeLabel.text == "Wood"{
                vc.genderData = WOOD.SIZE
            }else if self.structureTypeLabel.text == "Steel A Frame"{
                vc.genderData = STEEL.SIZE
            }else if self.structureTypeLabel.text == "Steel Multi-Mast"{
                vc.genderData = STEEL_MM.SIZE
            }else if self.structureTypeLabel.text == "Steel Monopole"{
                if self.subTypeLabel.text == "Tri-Sided Stacked Center Mounted" || self.subTypeLabel.text == "Tri-Sided Center Mounted"{
                    vc.genderData = ["672"]
                }else{
                    vc.genderData = MONOPOLE.SIZE
                }
            }
            vc.sourceView = BUTTON_TYPE_ENUM.DISPLAY_SIZE_TYPE
            
            break
        case 3:
            if self.structureTypeLabel.text == "Wood"{
                vc.genderData = WOOD.HAGL
            }else if self.structureTypeLabel.text == "Steel A Frame"{
                vc.genderData = STEEL.HAGL
            }else if self.structureTypeLabel.text == "Steel Multi-Mast"{
                if self.subTypeLabel.text == "Single Face" {
                    vc.genderData = ["0-20","21-30","31-40"]
                }
                else {
                vc.genderData = STEEL_MM.HAGL
                }
            }
            else if self.structureTypeLabel.text == "Steel Monopole"{
                
                if self.subTypeLabel.text == "Tri-Sided Center Mounted" {
                    vc.genderData = ["31-40","56-80","80+"]
                }
            
                else if self.subTypeLabel.text == "Tri-Sided Stacked Center Mounted"{
                    vc.genderData = ["31-40"]
                }
                    else if self.subTypeLabel.text == "Single Face Center Mounted" && self.displaySizeLabel.text == "300" {
                        vc.genderData = ["0-20","21-30","31-40","56-80" ]
                    }
                else if self.subTypeLabel.text == "Single Face Center Mounted" && self.displaySizeLabel.text == "378" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                else if self.subTypeLabel.text == "Single Face Center Mounted" && self.displaySizeLabel.text == "480" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                else if self.subTypeLabel.text == "Single Face Partial Flag" && self.displaySizeLabel.text == "300" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                else if self.subTypeLabel.text == "Single Face Partial Flag" && self.displaySizeLabel.text == "378" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                else if self.subTypeLabel.text == "Single Face Partial Flag" && self.displaySizeLabel.text == "480" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                    
                else if self.subTypeLabel.text == "Single Face Full Flag" && self.displaySizeLabel.text == "300" {
                    vc.genderData = ["0-20","21-30","31-40"]
                }
                else if self.subTypeLabel.text == "Single Face Full Flag" && self.displaySizeLabel.text == "378" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                else if self.subTypeLabel.text == "Single Face Full Flag" && self.displaySizeLabel.text == "480" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                    
                else if self.subTypeLabel.text == "Double & V Face Center Mounted" && self.displaySizeLabel.text == "300" {
                    vc.genderData = ["0-20","21-30","31-40"]
                }
                    
                else if self.subTypeLabel.text == "Double & V Face Center Mounted" && self.displaySizeLabel.text == "378" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                    
                else if self.subTypeLabel.text == "Double & V Face Center Mounted" && self.displaySizeLabel.text == "480" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                
                else if self.subTypeLabel.text == "Double & V Face Partial Flag" && self.displaySizeLabel.text == "300" {
                    vc.genderData = ["0-20","21-30","31-40"]
                }
                else if self.subTypeLabel.text == "Double & V Face Partial Flag" && self.displaySizeLabel.text == "378" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                else if self.subTypeLabel.text == "Double & V Face Partial Flag" && self.displaySizeLabel.text == "480" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                else if self.subTypeLabel.text == "Double & V Face Full Flag" && self.displaySizeLabel.text == "300" {
                    vc.genderData = ["0-20","21-30","31-40"]
                }
                else if self.subTypeLabel.text == "Double & V Face Full Flag" && self.displaySizeLabel.text == "378" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                else if self.subTypeLabel.text == "Double & V Face Full Flag" && self.displaySizeLabel.text == "480" {
                    vc.genderData = ["0-20","21-30","31-40","56-80" ]
                }
                    
                else{
                    vc.genderData = MONOPOLE.HAGL
                }
            }
            vc.sourceView = BUTTON_TYPE_ENUM.HAGL_TYPE
            break
        case 4:
            vc.genderData = DROP_DOWN_ARRAYS.DISPLAY_ELEMENT_ARRAY
            vc.sourceView = BUTTON_TYPE_ENUM.DISPLAY_ELEMENT_TYPE
            break
        case 5:
            vc.genderData = DROP_DOWN_ARRAYS.ILLUMINATION_TYPE_ARRAY
            vc.sourceView = BUTTON_TYPE_ENUM.ILLUMINATION_TYPE
            break
        case 6:
            vc.genderData = DROP_DOWN_ARRAYS.YEAR_BUILT_ARRAY
            vc.sourceView = BUTTON_TYPE_ENUM.YEAR_BUILT_TYPE
            break
        default:
            break
        }
    }
    
    func isAllFourAvailable() -> Bool{
        if self.structureTypeLabel.text != "Structure Type" && self.subTypeLabel.text !=  "Sub Type" && self.displaySizeLabel.text != "Display Size (Upto X sf)" && self.HAGL_Label.text != "HAGL(Height above ground level)"{
            return true
        }
        return false
    }
    
    
    func calculateBaseCost(){
        let params = [
            "frame_type" : self.subTypeLabel.text!,
            "base_type" : self.structureTypeLabel.text!,
            "hagl" : self.HAGL_Label.text!,
            "size" : self.displaySizeLabel.text!
        ]
        print(params)
        BillboardWebServices.getBaseCost(params) { (success, error, baseCostData) in
            if success{
                if let data = baseCostData?.data{
                    print(data.base_cost)
                    self.baseCostTextField.text = data.base_cost
                }
            }
        }
    }
    
    func getAge(_ year : String) -> Int{
        //        let date = Date()
        //        let formatter = DateFormatter()
        //        formatter.dateFormat = "dd.MM.yyyy"
        //        let result = formatter.string(from: date)
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        if let currentYear =  components.year{
            if let intValYearBuilt = Int(year){
                let age = currentYear - intValYearBuilt
                return age
            }
        }
        return 0
    }
    
}
