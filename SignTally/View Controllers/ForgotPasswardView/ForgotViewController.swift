//
//  ForgotViewController.swift
//  SignTally
//
//  Created by orangemac05 on 29/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import SVProgressHUD

class ForgotViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        let error = self.loginValidation()
        if error == ""{
           self.forgotPassword()
        }else{
            showAlert(title: "SignTally", message: error, buttonTitle: "OK", vc: self)
        }
    }
    
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loginValidation() -> String{
        if self.emailTextField.text == "" {
            return VALIDATION_ENUM.BLANK_EMAIL_VALIDATION_MESSEGE
        }else if !isValidEmail(testStr: self.emailTextField.text ?? ""){
            return VALIDATION_ENUM.INVALID_EMAIL_MESSEGE
        }
        return ""
    }
    
    
    func forgotPassword(){
        self.view.endEditing(true)
        SVProgressHUD.show()
        let params = [
            "email_id": self.emailTextField.text!,
            "user_id":USER_ID()
        ]
        ProfileServices.ForgotPasswordUser(params) { (success, error, userData) in
            SVProgressHUD.dismiss()
            if success{
                if let loginData = userData?.data{
                    print(loginData)
                }
                self.dismiss(animated: true, completion: nil)
            }else{
                showAlert(title: "SignTally", message: error!, buttonTitle: "Ok", vc: self)
            }
        }
    }
    
}
