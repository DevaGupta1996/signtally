//
//  MapController.swift
//  SignTally
//
//  Created by Orange on 10/11/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import SVProgressHUD
import GoogleMaps
import CoreLocation


class MapController: UIViewController {
    var mapView : GMSMapView?
    var locationManager = CLLocationManager()
    var currnetLocation = CLLocation()
    var placePredections = [Place]()
    var geocoder = GMSGeocoder()
    var zipcode = ""
    var placeDiscription = ""
    
    
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var placeSearchTextField: UITextField!
    @IBOutlet weak var searchResultTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupLocationManager()
        self.setupMapView()
        //        self.getPastValuations()
    }
    
    func setupMapView(){
        self.mapView = GMSMapView(frame: CGRect(x: 0, y: 0, width: self.mapContainerView.frame.width, height: self.mapContainerView.frame.height))
        self.mapView?.isMyLocationEnabled = true
        self.mapView?.delegate = self
        self.mapContainerView.insertSubview(self.mapView!, at: 0)
    }
    
    
    func setupLocationManager(){
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.distanceFilter = 100
        self.locationManager.startUpdatingLocation()
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func useLocationAction(_ sender: UIButton) {
        let locationDict = ["zipcode" : self.zipcode,
                            "locationName" : self.placeDiscription
        ]
        
        //print(locationDict)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LOCATION"), object: nil, userInfo: locationDict)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func homeLocationAction(_ sender: UIButton) {
        self.mapView?.animate(toLocation: self.currnetLocation.coordinate)
    }
}

extension MapController : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestAlwaysAuthorization()
            break
        case .authorizedAlways,.authorizedWhenInUse:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted , .denied:
            let alert = UIAlertController(title: "Location permissions are disabled for this application", message: "Please enable your location services", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            }
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
            break
//        case .denied :
//            let alert = UIAlertController(title: "Device location services are disabled", message: "Please enable your location services", preferredStyle: .alert)
//            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
//                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
//            }
//            alert.addAction(okAction)
//            present(alert, animated: true, completion: nil)
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            self.currnetLocation = location
            let locationCoordinates = location.coordinate
            let camera = GMSCameraPosition(target: locationCoordinates, zoom: 16.0, bearing: 0, viewingAngle: 0)
            self.mapView?.camera = camera
            self.mapView?.animate(toLocation: locationCoordinates)
        }
    }
    
    
    func googleReversedGeocode(_ location : CLLocationCoordinate2D ){
        SVProgressHUD.show()
        geocoder.reverseGeocodeCoordinate(location) { (response, error) in
            SVProgressHUD.dismiss()
            if let locationData = response?.results(){
                let address : GMSAddress = locationData[0]
                if let locality = address.lines{
                    let completeAddress = locality.map{String($0)}//.joined(separator: ",")
                    self.placeDiscription = completeAddress.joined(separator: ",")
                }
            }
            print(error)
        }
    }
    
    
    
    
}

extension MapController : GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let latitude = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        geocode(latitude: latitude, longitude: longitude) { (placemark, error) in
            guard let placemark = placemark, error == nil else { return }
            DispatchQueue.main.async {
                print("address1:", placemark.thoroughfare ?? "")
                print("address2:", placemark.subThoroughfare ?? "")
                print("city:",     placemark.locality ?? "")
                print("state:",    placemark.administrativeArea ?? "")
                print("zip code:", placemark.postalCode ?? "")
                self.zipcode = placemark.locality ?? ""
                print("country:",  placemark.country ?? "")
            }
        }
        self.googleReversedGeocode(mapView.camera.target)
    }
}


extension MapController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.placePredections.count > 0{
            return self.placePredections.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let data = self.placePredections[indexPath.row]
        if let structure = data.structuredFormat{
            cell.textLabel?.text = structure.main_text
            cell.detailTextLabel!.text = structure.secondary_text
        }
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let searchPlace = self.placePredections[indexPath.row]
        print(searchPlace)
        self.view.endEditing(true)
        self.placeDiscription = searchPlace.description
        self.getPlaceCoordinates(searchPlace.description)
    }
    
    func getPlaceCoordinates(_ placeDescription : String){
        GoogleWebService.getPlaceCoordinates(placeName: placeDescription) { (success, error, coordinate) in
            if success{
                if let data = coordinate?.results[0].geometry{
                    if let latLong = data.location{
                        print(latLong.Latitude,latLong.Longitude)
                        self.searchResultTableView.alpha = 0
                        let coordinates = CLLocationCoordinate2DMake(latLong.Latitude, latLong.Longitude)
                        self.mapView?.animate(toLocation: coordinates)
                    }
                }
            }else{
                print(error!)
            }
        }
    }
}


extension MapController : UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let finalText = textField.text ?? "" + string
        if finalText != ""{
            let trimmedSearchText = textField.text!.replacingOccurrences(of: " ", with: "")
            GoogleWebService.getPlaceList(searchString: trimmedSearchText) { (success, error, place) in
                if success{
                    if let data = place{
                        self.placePredections = data.predictions
                        if data.predictions.count > 0{
                            self.searchResultTableView.alpha = 1
                            self.searchResultTableView.reloadData()
                        }else{
                            self.searchResultTableView.alpha = 0
                        }
                    }
                }else{
                    self.searchResultTableView.alpha = 0
                    print(error!)
                }
            }
        }else{
            self.searchResultTableView.alpha = 0
        }
        return true
    }
}
