//
//  HomeViewController.swift
//  SignTally
//
//  Created by orangemac05 on 29/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import CoreLocation
import NVActivityIndicatorView
import Alamofire

class HomeViewController: UIViewController {

    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var userNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.respondToSideMenuNotifiers()
        if let fName = Preferences.getFirstName(){
            self.userNameLabel.text = fName.uppercased()
        }
    }
    

    
    @IBAction func addSignButtonAction(_ sender: UIButton) {
        request(BASE_URL + "users/billboard/valuation/check/" + Preferences.getUserID()!, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted, headers: nil).validate().responseObject { (response: DataResponse<User>) in
            switch response.result {
            case .success(let val):
                if val.status == "200" {
                    if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.ADD_SIGN_IDENTIFIER)as? AddSignController{
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else {
                    let alert = UIAlertController(title: nil, message: val.msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            case .failure(_):
                print("api error")
            }
        }
    }
    
   
    @IBAction func pastValuationsButtonAction(_ sender: UIButton) {
        if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.PAST_VALUATIONS_IDENTIFIER)as? PastValuationController{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
   
    
    @IBAction func helpButtonAction(_ sender: UIButton) {
        if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.HELP_CONTROLLER_IDENTIFIER)as? HelpViewController{
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension HomeViewController{
    func respondToSideMenuNotifiers(){
        
        NotificationCenter.default.addObserver(forName:  NSNotification.Name(rawValue: SIDE_MENU_NOTIFIER.ADD_SIGN), object: nil, queue: OperationQueue.current) { (notification) in
            if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.ADD_SIGN_IDENTIFIER)as? AddSignController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        NotificationCenter.default.addObserver(forName:  NSNotification.Name(rawValue: SIDE_MENU_NOTIFIER.PAST_VALUATION), object: nil, queue: OperationQueue.current) { (notification) in
            if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.PAST_VALUATIONS_IDENTIFIER)as? PastValuationController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        
        NotificationCenter.default.addObserver(forName:  NSNotification.Name(rawValue: SIDE_MENU_NOTIFIER.PRIVACY_POLICY), object: nil, queue: OperationQueue.current) { (notification) in
            if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.PRIVACY_POLICY_INDENTIFIER)as? PrivacyPolicyController{
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        NotificationCenter.default.addObserver(forName:  NSNotification.Name(rawValue: SIDE_MENU_NOTIFIER.HELP), object: nil, queue: OperationQueue.current) { (notification) in
            if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.HELP_CONTROLLER_IDENTIFIER)as? HelpViewController{
                self.present(vc, animated: true, completion: nil)
            }
        }
        
        NotificationCenter.default.addObserver(forName:  NSNotification.Name(rawValue: SIDE_MENU_NOTIFIER.CHANGEPASSWARD), object: nil, queue: OperationQueue.current) { (notification) in
            if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.CHANGE_PASSWARD_IDENTIFIER)as? ChangePasswordViewController{
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        NotificationCenter.default.addObserver(forName:  NSNotification.Name(rawValue: SIDE_MENU_NOTIFIER.LOGOUT), object: nil, queue: OperationQueue.current) { (notification) in
            Preferences.removeAll()
            if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.LOGIN_IDENTIFIER)as? LoginController{
                APP_DELEGATE.window?.rootViewController = vc
            }
        }
    }
    
}
