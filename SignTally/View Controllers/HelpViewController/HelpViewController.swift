//
//  HelpViewController.swift
//  SignTally
//
//  Created by orangemac05 on 30/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {

    @IBOutlet weak var helpTextView: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()        
    }
    

    
    @IBAction func helpButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
