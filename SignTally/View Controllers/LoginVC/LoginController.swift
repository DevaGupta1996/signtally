//
//  LoginController.swift
//  SignTally
//
//  Created by Orange on 29/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import SVProgressHUD
import NVActivityIndicatorView


class LoginController: UIViewController {
    
    @IBOutlet weak var emailAddressTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var signInButton: UIButton!
    
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func forgotButtonAction(_ sender: UIButton) {
        if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.FORGOT_PASSWORD_IDENTIFIER)as? ForgotViewController{
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func signInButtonAction(_ sender: UIButton) {
        
       let error = self.loginValidation()
        if error == ""{
            self.userLogin()
        }else{
            showAlert(title: "SignTally", message: error, buttonTitle: "OK", vc: self)
        }
        
    
       
    }
    
 @IBAction func SignUpButtonAction(_ sender: UIButton) {
     if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.SIGNUP_IDENTIFIER)as? SignUpViewController{
       self.present(vc, animated: true, completion: nil)
      }
 }
    
  // LOGIN VALIDATION CHECK
    func loginValidation() -> String{
        if self.emailAddressTextField.text == "" && self.passwordTextField.text == ""{
            return VALIDATION_ENUM.ALL_FIELD_VALIDATION_MESSEGE
        }else if self.emailAddressTextField.text == "" {
            return VALIDATION_ENUM.BLANK_EMAIL_VALIDATION_MESSEGE
        }else if !isValidEmail(testStr: self.emailAddressTextField.text ?? ""){
            return VALIDATION_ENUM.INVALID_EMAIL_MESSEGE
        }
        else if self.passwordTextField.text == ""{
            return VALIDATION_ENUM.BLANK_PASSWARD_VALIDATION_MESSEGE
        }
        return ""
    }
    
   // LOGIN METHODS
    func userLogin(){
        self.view.endEditing(true)
        SVProgressHUD.show()
        let params = [
            "email_id" : self.emailAddressTextField.text!,
            "password" : self.passwordTextField.text!
        ]
        ProfileServices.LoginUser(params) { (success, error, userData) in
            SVProgressHUD.dismiss()
            if success{
                if let loginData = userData?.data{
                    print(loginData)
                    Preferences.setEmail(name: loginData.email_id)
                    Preferences.setUserID(name: loginData._id)
                    Preferences.setFirstName(name: loginData.first_name)
                    Preferences.setLastName(name: loginData.last_name)
                    Preferences.setProfilePicture(name: loginData.logo_url)
                }
                
                if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.HOME_NAVIGATION_IDENTIFIER)as? UINavigationController{
                    APP_DELEGATE.window?.rootViewController = vc
                }
            }else{
                showAlert(title: "SignTally", message: error!, buttonTitle: "Ok", vc: self)
            }
        }
        
        
    }
}
