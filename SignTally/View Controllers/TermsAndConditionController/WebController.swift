//
//  WebController.swift
//  SignTally
//
//  Created by Orange on 20/11/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class WebController: UIViewController {

    @IBOutlet weak var webContainerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.webContainerView.frame.height))
        webView.navigationDelegate = self
        let url = URL(string: "https://www.signtally.com/terms-and-conditions.html")
        webView.load(URLRequest(url: url!))

        self.webContainerView.addSubview(webView)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    

}

extension WebController : WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SVProgressHUD.show()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
    }
}
