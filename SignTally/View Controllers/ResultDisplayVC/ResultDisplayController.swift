//
//  ResultDisplayController.swift
//  SignValue
//  Created by orangemac05 on 29/10/18.
//  Copyright © 2018 Orange. All rights reserved.


import UIKit
import Alamofire
import AlamofireImage

class ResultDisplayController: UIViewController {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var otherCompanyLabel: UILabel!
    @IBOutlet weak var displayNumberLabel: UILabel!
    @IBOutlet weak var permitNumberLabel: UILabel!
    @IBOutlet weak var streetNumberAndNameLabel: UILabel!
    @IBOutlet weak var latLongLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var structureTypeLabel: UILabel!
    @IBOutlet weak var subTypeLabel: UILabel!
    @IBOutlet weak var DisplaySizeLabel: UILabel!
    @IBOutlet weak var HAGL_Label: UILabel!
    @IBOutlet weak var additionalCostLabel: UILabel!
    @IBOutlet weak var stackedByLabel: UILabel!
    @IBOutlet weak var illuminatedLabel: UILabel!
    @IBOutlet weak var yearBuiltLabel: UILabel!
    
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var noDiditalDisplayLabel: UILabel!
    @IBOutlet weak var threeMsgDiaplayLabel: UILabel!
    @IBOutlet weak var threeDisplayBuiltLabel: UILabel!
    
    @IBOutlet weak var structureRLNCD_LABEL: UILabel!
    @IBOutlet weak var digitalDisplayRLNCD_Label: UILabel!
    @IBOutlet weak var threeMsgDisplayRLNCD_Label: UILabel!
    @IBOutlet weak var totalBillboardRLNCD_Label: UILabel!
    @IBOutlet weak var commentTextField: UITextView!
    @IBOutlet weak var imageViewFirst: UIImageView!
    @IBOutlet weak var imageViewSecond: UIImageView!
    @IBOutlet weak var yearDigitalDisplayBuildLabel: UILabel!
    
    
    
    var billboardData : BillboardData?
    var total_bast_cost : Total_Base_cost?
    var comment = ""
    var source = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // DISPLAY VALUATION DETAIL AFTER GETTING FROM PREVIOUS CONTROLLER
        self.displayBillboardData()
    }
    
    
    func displayBillboardData(){
        if let data = self.billboardData{
            // DATE FORMATTING
            if let index = data.created_on.range(of: "T")?.lowerBound {
                let substring = data.created_on[..<index]
                let string = String(substring)
                print(string)
                if let date = dateSignTally(fromString: string){
                    if let finalDate = date.getHumanReadableMedium(){
                        self.dateLabel.text = finalDate
                    }
                }
            }
            
            print("data..........>",data)
            self.companyNameLabel.text = data.sigh
            self.otherCompanyLabel.text = data.other_company
            self.displayNumberLabel.text = data.display_number
            self.permitNumberLabel.text = data.permit_number
            self.streetNumberAndNameLabel.text = "\(data.street_no),\(data.street_name)"
            self.latLongLabel.text = "\(data.latitude),\(data.longitude)"
            self.cityLabel.text = data.city
            self.stateLabel.text = data.state
            self.structureTypeLabel.text = data.structure_type
            self.subTypeLabel.text = data.sub_type
            self.DisplaySizeLabel.text = data.display_size
            self.HAGL_Label.text = data.hagl
            if data.additional_cost != ""{
                self.additionalCostLabel.text = "$\(data.additional_cost)"
            }else{
                self.additionalCostLabel.text = ""
            }
            
            self.stackedByLabel.text = data.display_element
            self.illuminatedLabel.text = data.illuminated
            self.yearBuiltLabel.text = data.year_built_2
            
            self.ageLabel.text = data.age
            self.noDiditalDisplayLabel.text = data.digital_display
            self.threeMsgDiaplayLabel.text = data.three_message_display
            self.threeDisplayBuiltLabel.text = data.year_three_message_display
            
            if self.source == VIEW_CONTROLLER_IDENTIFIER.PAST_VALUATIONS_IDENTIFIER{
                self.structureRLNCD_LABEL.text = "$\(data.structure_rcnld)"
                self.digitalDisplayRLNCD_Label.text = "$\(data.digital_display_rcnld)"
                self.threeMsgDisplayRLNCD_Label.text = "$\(data.three_msg_display_rcnld)"
                self.totalBillboardRLNCD_Label.text = "$\(data.total_billboard_rcnld)"
            }else{
                if let costData = self.total_bast_cost{
                    self.structureRLNCD_LABEL.text = "$\(costData.structure_rcnld)"
                    self.digitalDisplayRLNCD_Label.text = "$\(costData.digital_display_rcnld)"
                    self.threeMsgDisplayRLNCD_Label.text = "$\(costData.three_msg_display_rcnld)"
                    self.totalBillboardRLNCD_Label.text = "$\(costData.total_billboard_evaluation)"
                }
            }
            
            self.threeMsgDiaplayLabel.text = "\(data.three_message_display)"
            self.yearDigitalDisplayBuildLabel.text = "\(data.year_digital_built)"
            
            if self.comment != ""{
                self.commentTextField.text = self.comment
            }else{
                self.commentTextField.text = data.comments
            }
            //
            
            if data.photo_url != ""{
                self.imageViewFirst.af_setImage(withURL: URL(string: data.photo_url)!)
            }
            
            if data.photo_url_2 != ""{
                self.imageViewSecond.af_setImage(withURL: URL(string: data.photo_url_2)!)
            }
        }
    }
    
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneButtonAction(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    func getDate() -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: Date())
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd-MMM-yyyy"
        let myStringafd = formatter.string(from: yourDate!)
        print(myStringafd)
        return myStringafd
    }
}
