//
//  SignUpViewController.swift
//  SignTally
//  Created by orangemac05 on 29/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import SVProgressHUD

class SignUpViewController: UIViewController {
    
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswardTextField: UITextField!
    @IBOutlet weak var checkButton: UIButton!
    var isTermsAndCondition = false

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        let validationMsg = self.signUpValidation()
        if validationMsg == ""{
            let alert = UIAlertController(title: "A link has been sent to your email id. Please click on the link to verify your account with us.", message: "Thanks Team SignTally", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { (action) in
                self.userSignup()
               
            }))
            self.present(alert, animated: true, completion: nil)
        
        }else{
            showAlert(title: "SignTally", message: validationMsg, buttonTitle: "OK", vc: self)
        }
    }
    
    @IBAction func acceptTermsandAction(_ sender: UIButton) {
        if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.WEB_IDENTIFIER)as? WebController{
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func checkboxAction(_ sender: UIButton) {
        if sender.image(for: .normal) == nil{
            sender.setImage(#imageLiteral(resourceName: "Image"), for: .normal)
            self.isTermsAndCondition = true
        }else{
            sender.setImage(nil, for: .normal)
            self.isTermsAndCondition = false
        }
    }
    
    // SIGNUP VALIDATION METHOD
    func signUpValidation() -> String{
        if self.firstNameTextField.text == "" && self.lastNameTextField.text == "" && self.emailAddressTextField.text == "" && self.passwordTextField.text == "" && self.confirmPasswardTextField.text == ""{
            return VALIDATION_ENUM.ALL_FIELD_VALIDATION_MESSEGE
        }else if self.firstNameTextField.text == ""{
            return VALIDATION_ENUM.BLANK_FIRST_NAME_VALIDATION_MESSEGE
        }else if self.lastNameTextField.text == ""{
            return VALIDATION_ENUM.BLANK_LAST_NAME_VALIDATION_MESSEGE
        }else if self.emailAddressTextField.text == "" {
            return VALIDATION_ENUM.BLANK_EMAIL_VALIDATION_MESSEGE
        }else if !isValidEmail(testStr: self.emailAddressTextField.text ?? ""){
            return VALIDATION_ENUM.INVALID_EMAIL_MESSEGE
        }else if self.passwordTextField.text == ""{
            return VALIDATION_ENUM.BLANK_PASSWARD_VALIDATION_MESSEGE
        }else if self.confirmPasswardTextField.text == ""{
            return VALIDATION_ENUM.BLANK_CONFIRM_PASSWORD_VALIDATION_MESSEGE
        }else if self.confirmPasswardTextField.text != self.passwordTextField.text {
            return VALIDATION_ENUM.BLANK_CHECK_PASSWORD_VALIDATION_MESSEGE
        }else if !self.isTermsAndCondition{
            return VALIDATION_ENUM.CHECK_T_AND_C
        }
        return ""
    }
    
    
    // SIGNUP METHOD
    func userSignup(){
        self.view.endEditing(true)
        SVProgressHUD.show()
        let params = [
            "email_id" : self.emailAddressTextField.text!,
            "password" : self.passwordTextField.text!,
            "re_enter_password" : self.confirmPasswardTextField.text!,
            "first_name" : self.firstNameTextField.text!,
            "last_name" : self.lastNameTextField.text!,
            "device_token" : DEVICE_TOKEN
        ]
        ProfileServices.SignupUser(params) { (success, error, userData) in
            SVProgressHUD.dismiss()
            if success{
                if let loginData = userData?.data{
                    print(loginData)
                    Preferences.setEmail(name: loginData.email_id)
                    Preferences.setUserID(name: loginData._id)
                    Preferences.setFirstName(name: loginData.first_name)
                    Preferences.setLastName(name: loginData.last_name)
                    Preferences.setProfilePicture(name: loginData.logo_url)
                }
                
                
                if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.LOGIN_IDENTIFIER)as? LoginController{
                    APP_DELEGATE.window?.rootViewController = vc
                }
            }else{
                showAlert(title: "SignTally", message: error!, buttonTitle: "Ok", vc: self)
            }
        }
    }
}
