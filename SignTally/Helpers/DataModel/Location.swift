//
//  Location.swift
//  SignTally
//
//  Created by Orange on 01/11/18.
//  Copyright © 2018 Orange. All rights reserved.
//




import Foundation
import UIKit
import ObjectMapper


struct Places : Mappable {
    var predictions = [Place]()
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        predictions <- map["predictions"]
    }
}

struct Place : Mappable {
    var description = ""
    var place_id = ""
    var structuredFormat : structured_formatting?
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        description <- map["description"]
        place_id  <- map["place_id"]
        structuredFormat <- map["structured_formatting"]
    }
}

struct  structured_formatting : Mappable {
    var main_text = ""
    var secondary_text = ""
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        main_text <- map["main_text"]
        secondary_text  <- map["secondary_text"]
    }
}

struct Coordinates: Mappable {
    var results = [LocationResult]()
    var status = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        results <- map["results"]
        status <- map["status"]
    }
}


struct LocationResult: Mappable {
    var geometry : Geometry!
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        geometry <- map["geometry"]
    }
}

struct Geometry : Mappable {
    var location : Location!
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        location <- map["location"]
    }
}

struct Location : Mappable {
    var Latitude = 0.0
    var Longitude = 0.0
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        Latitude <- map["lat"]
        Longitude <- map["lng"]
    }
}
