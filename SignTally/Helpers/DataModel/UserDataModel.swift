//
//  UserDataModel.swift
//  SignTally
//
//  Created by Orange on 31/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import ObjectMapper

struct User : Mappable{
    var status = ""
    var msg = ""
    var data : UserData?
    init?(map: Map) {
        
    }
    mutating func mapping(map: Map) {
        status <- map["status"]
        msg <- map["message"]
        data <- map["data"]
    }
}


struct UserData : Mappable{
    var _id = ""
    var first_name = ""
    var email_id  = ""
    var password = ""
    var user_token = ""
    var created_on  = ""
    var device_token = ""
    var is_email_verified = ""
    var is_deleted  : Bool?
    var logo_url = ""
    var user_type = ""
    var last_name  = ""
    var base_cost = ""
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        _id <- map["_id"]
        first_name <- map["first_name"]
        email_id <- map["email_id"]
        password <- map["password"]
        user_token <- map["user_token"]
        created_on <- map["created_on"]
        device_token <- map["device_token"]
        is_email_verified <- map["is_email_verified"]
        is_deleted <- map["is_deleted"]
        logo_url <- map["logo_url"]
        user_type <- map["user_type"]
        last_name <- map["last_name"]
        base_cost <- map["base_cost"]
        
        
    }
}
