//
//  ProfileWebService.swift
//  SignTally
//
//  Created by Orange on 31/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import Alamofire



class ProfileServices{
    
    class func LoginUser(_ params : Parameters, completion: @escaping (_ success: Bool,_ message: String?,_ pastValuationData: User?)->()) {
        print("\(BASE_URL)\(WEB_SERVICE_URL.login)")
        request("\(BASE_URL)\(WEB_SERVICE_URL.login)", method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted).responseObject { (response: DataResponse<User>) in
            switch response.result {
            case .success(let val):
                if val.status == "200" {
                    completion(true, nil, val)
                }else {
                    completion(false, val.msg, nil)
                }
            case .failure(let error):
                completion(false, error.localizedDescription, nil)
            }
        }
    }
    
    
    class func SignupUser(_ params : Parameters, completion: @escaping (_ success: Bool,_ message: String?,_ pastValuationData: User?)->()) {
        print("\(BASE_URL)\(WEB_SERVICE_URL.signUp)")
        request("\(BASE_URL)\(WEB_SERVICE_URL.signUp)", method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted).responseObject { (response: DataResponse<User>) in
            switch response.result {
            case .success(let val):
                if val.status == "200" {
                    completion(true, nil, val)
                }else {
                    completion(false, val.msg, nil)
                }
            case .failure(let error):
                completion(false, error.localizedDescription, nil)
            }
        }
    }
    
    
    class func ChangePasswordUser(_ params : Parameters, completion: @escaping (_ success: Bool,_ message: String?,_ pastValuationData: User?)->()) {
        print("\(BASE_URL)\(WEB_SERVICE_URL.changePassword)")
        request("\(BASE_URL)\(WEB_SERVICE_URL.changePassword)", method: .put, parameters: params, encoding: JSONEncoding.prettyPrinted).responseObject { (response: DataResponse<User>) in
            switch response.result {
            case .success(let val):
                if val.status == "200" {
                    completion(true, nil, val)
                }else {
                    completion(false, val.msg, nil)
                }
            case .failure(let error):
                completion(false, error.localizedDescription, nil)
            }
        }
    }

    class func ForgotPasswordUser(_ params : Parameters, completion: @escaping (_ success: Bool,_ message: String?,_ pastValuationData: User?)->()) {
        print("\(BASE_URL)\(WEB_SERVICE_URL.forgot)")
        request("\(BASE_URL)\(WEB_SERVICE_URL.forgot)", method: .put, parameters: params, encoding: JSONEncoding.prettyPrinted).responseObject { (response: DataResponse<User>) in
            switch response.result {
            case .success(let val):
                if val.status == "200" {
                    completion(true, nil, val)
                }else {
                    completion(false, val.msg, nil)
                }
            case .failure(let error):
                completion(false, error.localizedDescription, nil)
            }
        }
    }
}




