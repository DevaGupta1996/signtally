//
//  GoogleWebServices.swift
//  SignTally
//  Created by Orange on 01/11/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import Alamofire

class GoogleWebService{
    
    class func getPlaceList(searchString:String,completion : @escaping(_ success:Bool,_ error:String?,_ user:Places?)->()) {
        print("https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(GOOGLE_MAP_API_KEY)&input=\(searchString)")
        request("https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(GOOGLE_MAP_API_KEY)&input=\(searchString)", method: .get, parameters: nil , encoding: JSONEncoding.prettyPrinted).responseObject { (response: DataResponse<Places>) in
            switch response.result{
            case .success(let val):
                if val.predictions.count != 0 {
                    completion(true,nil,val)
                }else{
                    completion(false,"No Places Found",nil)
                }
            case .failure(let error):
                completion(false, error.localizedDescription, nil)
            }
        }
    }
    
    class func getPlaceCoordinates(placeName:String,completion : @escaping(_ success:Bool,_ error:String?,_ user:Coordinates?)->()) {
        let placeNameWithoutSpace = placeName.replacingOccurrences(of: " ", with: "%20")
        print("https://maps.google.com/maps/api/geocode/json?sensor=false&key=\(GOOGLE_MAP_API_KEY)&address=\(placeNameWithoutSpace)")
        request("https://maps.google.com/maps/api/geocode/json?sensor=false&key=\(GOOGLE_MAP_API_KEY)&address=\(placeNameWithoutSpace)", method: .get, parameters: nil , encoding: JSONEncoding.prettyPrinted).responseObject { (response: DataResponse<Coordinates>) in
            switch response.result{
            case .success(let val):
                if val.status == "OK" {
                    completion(true,nil,val)
                }else{
                    completion(false,"No Data Found",nil)
                }
            case .failure(let error):
                completion(false, error.localizedDescription, nil)
            }
        }
    }
}
